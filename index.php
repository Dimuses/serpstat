<?php

require __DIR__ . "/vendor/autoload.php";
$container = require __DIR__ . "/config/container.php";
require  __DIR__ . "/config/bootstrap.php";

$command = $argv[1] ?? null;
$param = $argv[2] ?? null;

/** @var \App\commands\CommandAbstract $func */
try{
    $func = $container->get($command);
    $func->withParam($param);
    $func->run();
}catch (Exception $e){
    echo "Unknown command. Use 'help' command to print all allowed commands";
}


