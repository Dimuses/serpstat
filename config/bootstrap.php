<?php

use App\core\Container;

define('REPORT_PATH', __DIR__ . '/../src/store/');

$container = Container::getInstance();
$db = $container->get('db');
$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

try{
    $db->exec('CREATE TABLE IF NOT EXISTS domain(id INTEGER PRIMARY KEY AUTO_INCREMENT, url varchar(255))');
    $db->exec('CREATE TABLE IF NOT EXISTS page(id INTEGER PRIMARY KEY AUTO_INCREMENT, url varchar(255), domain_id INTEGER, FOREIGN KEY (domain_id) REFERENCES domain(id) ON DELETE CASCADE )');
    $db->exec('CREATE TABLE IF NOT EXISTS image(id INTEGER PRIMARY KEY AUTO_INCREMENT, url varchar(255), page_id INTEGER, FOREIGN KEY (page_id) REFERENCES page(id) ON DELETE CASCADE )');
} catch (PDOException $e){
    echo $e->getMessage();
}