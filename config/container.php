<?php

use App\commands\Help;
use App\commands\Parse;
use App\commands\Report;
use App\core\Container;
use App\adapters\CurlAdapter;
use App\observers\SaveLocalLinks;
use App\observers\SavePictures;
use App\services\ParserService;

$container = Container::getInstance();

$container->set('db', function (Container $container) {
    return new PDO('mysql:host=localhost;dbname=netpeak', 'root');
});
$container->set('parse', function () use ($container) {
    $parse = new Parse(
        new CurlAdapter(),
        new ParserService()
    );
    $parse->attach(new SavePictures(
        new ParserService()
    ));
    $parse->attach(new SaveLocalLinks(
        new ParserService()
    ));
    return $parse;
});
$container->set('report', function () {
    return new Report();
});
$container->set('help', function () {
    return new Help();
});

return $container;