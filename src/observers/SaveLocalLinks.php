<?php

namespace App\observers;


use App\commands\Parse;
use App\services\ParserService;
use DOMDocument;

class SaveLocalLinks extends ParseObserver
{
    private $service;

    /**
     * SaveLocalLinks constructor.
     * @param ParserService $service
     */
    public function __construct(ParserService $service)
    {
        $this->service = $service;
    }

    public function run(Parse $object)
    {
        $result = null;
        $html = $object->getParsedHtml();
        $dom = new DOMDocument;
        @$dom->loadHTML($html);

        $urls = $this->service->saveLinks($dom, $object->getPageId());
        foreach ($urls as $href) {
            $url = $object->getScheme() . '://' . $object->getDomain() . $href;

            if ($url != $object->getUrl()) {
                $object->setUrl($url);
                $object->delegate();
            }

        }

    }
}