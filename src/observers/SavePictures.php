<?php

namespace App\observers;

use App\commands\Parse;
use App\services\ParserService;
use DOMDocument;
use Exception;


/**
 * Class SavePictures
 * @package App\observers
 */
class SavePictures extends ParseObserver
{
    private $service;
    /**
     * SaveLocalLinks constructor.
     * @param ParserService $service
     */
    public function __construct(ParserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Parse $object
     */
    public function run(Parse $object)
    {
        $html = $object->getParsedHtml();
        $dom = new DOMDocument;
        @$dom->loadHTML($html);
        $domain = $object->getScheme() . '://' . $object->getDomain();

        $this->service->savePictures($dom, $object->getPageId(), $domain);
    }
}