<?php
namespace App\observers;

use App\commands\Parse;

/**
 * Interface ParseObserver
 * @package src\observers
 */
abstract class ParseObserver
{


    /**
     * @param Parse $object
     * @return mixed
     */
    abstract function run(Parse $object);
}