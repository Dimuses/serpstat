<?php

namespace App\commands;

use App\adapters\Request;
use App\observers\ParseObserver;
use App\services\ParserService;

/**
 * Class Parse
 * @package src\commands
 */
class Parse extends CommandAbstract
{
    /**
     * @var string
     */
    protected $description = 'Запускает парсер, принимает обязательный параметр url (как с протоколом, так и без).';
    /**
     * @var array|\SplObjectStorage
     */
    protected $observers = [];
    /**
     * @var Request
     */
    protected $requestHandler;
    /**
     * @var
     */
    private $url;
    /**
     * @var
     */
    private $parsedHtml;
    /**
     * @var
     */
    private $pageId;
    /**
     * @var
     */
    private $domain;
    /**
     * @var
     */
    private $scheme;
    /**
     * @var
     */
    private $domainId;
    /**
     * @var
     */
    private $service;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return mixed
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * Parse constructor.
     * @param Request $request
     * @param ParserService $service
     */
    public function __construct(Request $request, ParserService $service)
    {
        $this->observers = new \SplObjectStorage();
        $this->requestHandler = $request;
        $this->service = $service;
    }

    /**
     * @param ParseObserver $observer
     */
    public function attach(ParseObserver $observer)
    {
        $this->observers->attach($observer);
    }

    /**
     * @param ParseObserver $observer
     */
    public function detach(ParseObserver $observer)
    {
        $this->observers->detach($observer);
    }

    /**
     * @return mixed
     */
    public function getParsedHtml()
    {
        return $this->parsedHtml;
    }

    /**
     *
     */
    public function notify()
    {
        /** @var ParseObserver $observer */
        foreach ($this->observers as $observer) {
            $observer->run($this);
        }
    }

    /**
     * @param $param
     * @return $this
     */
    public function withParam($param)
    {
        if(strpos($param, 'http') === false){
            $url = 'http://' . $param;
        }
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     *
     */
    public function run()
    {
        try {
            $this->delegate();
            $this->report();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     *
     */
    public function delegate()
    {
        $this->parsedHtml = $this->requestHandler->make($this->url);
        if (!$this->domain) {
            $this->domain = parse_url($this->url, PHP_URL_HOST);
            $this->scheme = parse_url($this->url, PHP_URL_SCHEME);
            $this->service->deleteDomainByUrl($this->domain);
            $this->domainId = $this->service->addDomain($this->domain);
        }
        $pageId = $this->service->addPage($this->url, $this->domainId);
        if($pageId){
            $this->pageId = $pageId;
            $this->notify();
        }
    }

    private function report()
    {
        $filename = REPORT_PATH . $this->domain . '.csv';
        $file = fopen($filename, 'w');
        fputcsv($file, ['page_url', 'image_url'], ';');

        try {
            $res = $this->service->getReportData($this->domain);
        } catch (\Exception $e) {
            $res = [];
        }

        foreach ($res as $row) {
            fputcsv($file, [$row['page_url'], $row['image_url']], ';');
        }

        echo realpath($filename);
    }



}