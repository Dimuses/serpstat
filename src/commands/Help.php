<?php
namespace App\commands;


use App\core\Container;

/**
 * Class Help
 * @package App\commands
 */
class Help extends CommandAbstract
{
    /**
     * @var string
     */
    protected $description = 'Выводит список команд с пояснениями';

    /**
     * @var array
     */
    public  $commands = ['parse', 'report', 'help'];


    /**
     *
     */
    function run()
    {
        $container = Container::getInstance();
        foreach ($this->commands as $command){
            try {
                echo $command . ': ' . $container->get($command)->getDescription() . PHP_EOL;
            } catch (\Exception $e) {
            }
        }
    }
    /**
     * @param $param
     */
    function withParam($param)
    {
    }
}