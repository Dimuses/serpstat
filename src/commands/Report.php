<?php
namespace App\commands;

use App\commands\CommandAbstract;

class Report extends CommandAbstract
{
    private $domain;

    function run()
    {
        $path = REPORT_PATH . $this->domain . '.csv';
        if(file_exists($path)){
            echo realpath($path);
        }else{
            echo 'No report';
        }
    }

    function withParam($param)
    {
        if(strpos($param,'http') === false){
            $this->domain = $param;
        }else{
            $this->domain = parse_url($param, PHP_URL_HOST);
        }
    }

    protected $description = 'Выводит в консоль результаты анализа для домена, принимает обязательный параметр domain (как с протоколом, так и без).';
}