<?php

namespace App\commands;

use App\database\DbDriver;

/**
 * Class CommandAbstract
 */
abstract class CommandAbstract
{
    /**
     * @var string
     */
    protected $description = '';

    /**
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }
    /**
     *
     */
    abstract function run();

    abstract function withParam($param);
}