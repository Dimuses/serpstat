<?php
namespace App\adapters;


class GuzzleAdapter implements Request
{
    private $client;
    /**
     * RequestAdapter constructor.
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    public function make($url)
    {
        $response = $this->client->request('GET', $url, ['timeout' => 6.14]);
        return $response->getBody()->getContents();
    }
}