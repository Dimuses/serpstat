<?php
namespace App\adapters;


/**
 * Interface Request
 * @package src\adapters
 */
interface Request
{
    /**
     * @param $url
     * @return mixed
     */
    public function make($url);
}