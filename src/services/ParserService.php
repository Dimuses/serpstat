<?php


namespace App\services;


use App\core\Container;
use Exception;

/**
 * Class ParserService
 * @package App\services
 */
class ParserService
{
    /**
     * @param \DOMDocument $dom
     * @param $pageId
     * @param $domain
     */
    public function savePictures(\DOMDocument $dom, $pageId, $domain)
    {
        try {
            $db = Container::getInstance()->get('db');
            $stmt = $db->prepare("INSERT INTO image (url, page_id) VALUES (:url, :page_id)");

            foreach ($dom->getElementsByTagName('img') as $node) {
                /** @var \DOMElement $node */
                $src = $node->getAttribute('src');
                $res = $db->query("SELECT * FROM image WHERE page_id = '$pageId' and url = '$src'")->fetch();

                if (!$res[0] && $src != '' && strpos($src, '//') === false) {
                    /** @var \PDOStatement $stmt */
                    try {
                        $row['url'] = $domain . $src;
                        $row['page_id'] = $pageId ?: $this->getLastRecordId('page');
                        $stmt->execute($row);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param \DOMDocument $dom
     * @param $pageId
     * @return array
     */
    public function saveLinks(\DOMDocument $dom, $pageId)
    {
        $urls = [];
        /** @var \PDOStatement $stmt */
        foreach ($dom->getElementsByTagName('a') as $node) {
            /** @var \DOMElement $node */
            $href = $node->getAttribute('href');

            if ($href[0] == '#'
                || $href == '/'
                || strpos($href, 'http') !== false
                || strpos($href, 'tel') !== false
                || strpos($href, 'mailto') !== false
                || $href == ''
            ) {
                continue;
            } else {
                $urls[] = $href;
            }
        }
        return $urls;
    }

    /**
     * @param $table
     * @return null
     */
    public function getLastRecordId($table)
    {
        try {
            $db = Container::getInstance()->get('db');
            $res = $db->query("SELECT id FROM $table ORDER BY id DESC LIMIT 1;")->fetch();
            return $res[0];

        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param $domain
     * @return mixed
     */
    public function getReportData($domain)
    {
        try {
            $db = Container::getInstance()->get('db');
            $res = $db->query("SELECT page.url as page_url, image.url as image_url 
                           FROM domain
                           JOIN page  on (page.domain_id = domain.id)  
                           JOIN image on (image.page_id = page.id)  
                           WHERE domain.url = '$domain'")
                ->fetchAll();
            return $res;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteDomainByUrl($url)
    {
        try {
            $db = Container::getInstance()->get('db');
            return $db->prepare("DELETE FROM domain WHERE url = :url")
                ->execute(['url' => $url]);
        } catch (Exception $e) {
            return false;
        }

    }

    /**
     * @param $domain
     * @return
     */
    public function addDomain($domain)
    {
        try {
            $db = Container::getInstance()->get('db');
            $stmt = $db->prepare("INSERT INTO domain (url) VALUES (?)");
            /** @var \PDOStatement $stmt */
            $stmt->execute([$domain]);
            return $db->lastInsertId();
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * @param $url
     * @param $domainId
     * @return bool
     */
    public function addPage($url, $domainId)
    {
        try {
            $db = Container::getInstance()->get('db');
            $res = $db->query("SELECT * FROM page WHERE url = '$url'")->fetch();
            if (!$res) {
                $sql = "INSERT INTO page (url, domain_id) VALUES (?, ?)";
                $stmt = $db->prepare($sql);
                /** @var \PDOStatement $stmt */
                $stmt->execute([$url, $domainId]);
                return $db->lastInsertId();
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }
}